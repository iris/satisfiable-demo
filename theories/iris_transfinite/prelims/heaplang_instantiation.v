From iris.proofmode Require Import proofmode.
From iris.bi.lib Require Import fractional.
From demo.iris_transfinite.prelims Require Import weakestpre.
From iris.heap_lang Require Export class_instances.
From iris.heap_lang Require Import tactics notation.
From iris.prelude Require Import options.
From transfinite.base_logic.lib Require Import mono_nat invariants.
From transfinite.base_logic.lib Require Export gen_heap proph_map gen_inv_heap.
From demo.iris_transfinite.prelims Require Import transfinite.


Class heapGS Σ := HeapGS {
  heapGS_invGS : invG Σ;
  heapGS_gen_heapGS :> gen_heapG loc (option val) Σ;
  heapGS_inv_heapGS :> inv_heapG loc (option val) Σ;
  heapGS_proph_mapGS :> proph_mapG proph_id (val * val) Σ;
  heapGS_step_name : gname;
  heapGS_step_cnt : mono_natG Σ;
}.


Local Existing Instance heapGS_step_cnt.

Section steps.
  Context `{!heapGS Σ}.

  Local Definition steps_auth (n : nat) : iProp Σ :=
    mono_nat_auth_own heapGS_step_name 1 n.

  Definition steps_lb (n : nat) : iProp Σ :=
    mono_nat_lb_own heapGS_step_name n.

  Local Lemma steps_lb_valid n m :
    steps_auth n -∗ steps_lb m -∗ ⌜m ≤ n⌝.
  Proof.
    iIntros "Hauth Hlb".
    by iDestruct (mono_nat_lb_own_valid with "Hauth Hlb") as %[_ Hle].
  Qed.

  Local Lemma steps_lb_get n :
    steps_auth n -∗ steps_lb n.
  Proof. apply mono_nat_lb_own_get. Qed.

  Local Lemma steps_auth_update n n' :
    n ≤ n' → steps_auth n ==∗ steps_auth n' ∗ steps_lb n'.
  Proof. intros Hle. by apply mono_nat_own_update. Qed.

  Local Lemma steps_auth_update_S n :
    steps_auth n ==∗ steps_auth (S n).
  Proof.
    iIntros "Hauth".
    iMod (mono_nat_own_update with "Hauth") as "[$ _]"; [lia|done].
  Qed.

  Lemma steps_lb_le n n' :
    n' ≤ n → steps_lb n -∗ steps_lb n'.
  Proof. intros Hle. by iApply mono_nat_lb_own_le. Qed.

End steps.

Notation inv_heap_inv := (inv_heap_inv loc (option val)).



Global Program Instance heapGS_irisGS `{!heapGS Σ} : irisGS heap_lang Σ := {
  iris_invG := heapGS_invGS;
  state_interp σ step_cnt κs _ :=
    (gen_heap_interp σ.(heap) ∗ proph_map_interp κs σ.(used_proph_id) ∗
     steps_auth step_cnt)%I;
  fork_post _ := True%I
}.
Next Obligation.
  iIntros (?? σ ns κs nt)  "/= ($ & $ & H)".
  by iMod (steps_auth_update_S with "H") as "$".
Qed.
