From transfinite.base_logic Require Export own iprop.
From transfinite.stepindex Require Import ordinals.


(* We make ordinals, i.e., [ordI], the default step-indexing instance for everything that follows. *)
Global Existing Instance ordI | 1.