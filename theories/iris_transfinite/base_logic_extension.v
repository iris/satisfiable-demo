From iris.prelude Require Import options.
From iris.algebra Require Import auth.
From iris.algebra Require Import gmap_view gset coPset.
From iris.proofmode Require Import tactics.
From transfinite.base_logic Require Import own wsat invariants.


From demo.iris_transfinite Require Import satisfiable.
(** *** Satisfiability and Fancy Updates *)
(** We provide lemmas for
  - allocating the initial view [⊤]
  - eliminating fancy updates by updating the view

  These lemmas should eventually be moved into the fancy update machinery of Iris.
*)
Definition view `{!invG Σ} (E: coPset): iProp Σ := wsat ∗ ownE E.

Lemma SAT_alloc_fancy_updates `{!invPreG Σ} F P:
  SAT F [] P → ∃ _: invG Σ, SAT F [view ⊤] P.
Proof.
  intros Hsat.
  eapply (SAT_alloc_res (gmap_view_auth (DfracOwn 1) ∅)) in Hsat as [γI Hsat]; last first.
  { by apply gmap_view_auth_valid. }
  eapply (SAT_alloc_res (CoPset ⊤)) in Hsat as [γE Hsat]; last done.
  eapply (SAT_alloc_res (GSet ∅)) in Hsat as [γD Hsat]; last done.
  exists (InvG _ _ _ γI γE γD). rewrite SAT_res_cons.
  eapply SAT_mono, Hsat. rewrite /view /wsat /ownE -!lock.
  iIntros "(HD & HE & HI & $)"; iFrame.
  iExists ∅. rewrite fmap_empty big_opM_empty. by iFrame.
Qed.

Lemma SAT_fupd `{!invG Σ} E1 E2 F P:
  SAT F [view E1] (|={E1,E2}=> P)%I →
  SAT F [view E2] P.
Proof.
  rewrite SAT_res_cons. intros Hsat. rewrite SAT_res_cons.
  eapply SAT_except_0, SAT_upd, SAT_mono, Hsat.
  iIntros "[Hview HP]".
  rewrite fancy_updates.uPred_fupd_eq /fancy_updates.uPred_fupd_def.
  rewrite /view /= -bi.sep_assoc //. by iApply "HP".
Qed.


(* Global Ghost State Constructions *)

(* Ghost Maps *)
From transfinite.base_logic Require Import ghost_map.
Lemma SAT_ghost_map_alloc {Σ: gFunctors} {K V} `{Countable K} `{!ghost_mapG Σ K V} m F P :
  SAT F [] P → ∃ γ, SAT F [] (ghost_map_auth γ 1 m ∗ ([∗ map] k ↦ v ∈ m, k ↪[γ] v) ∗ P).
Proof.
  (* NOTE: this proof follows [ghost_map_alloc_strong] *)
  Local Existing Instance ghost_map_inG.
  rewrite ghost_map.ghost_map_auth_unseal /ghost_map.ghost_map_auth_def
          ghost_map.ghost_map_elem_unseal /ghost_map.ghost_map_elem_def.
  intros Hsat.
  apply (SAT_alloc_res (gmap_view_auth (V:=(leibnizO V)) (DfracOwn 1) ∅))
    in Hsat as [γ Hauth]; last first.
  { eapply gmap_view_auth_valid. }
  exists γ. eapply SAT_upd, SAT_mono, Hauth.
  iIntros "[Hauth $]". rewrite -big_opM_own_1 -own_op. iApply (own_update with "Hauth").
  etrans; first apply (gmap_view_alloc_big (V:=(leibnizO V)) _ m (DfracOwn 1)).
  - apply map_disjoint_empty_r.
  - done.
  - rewrite right_id //.
Qed.


(* Gen Heaps *)
From transfinite.base_logic.lib Require Import gen_heap ghost_map.

Local Notation "l ↦ v" := (gen_heap.mapsto l (DfracOwn 1) v) (at level 20) : bi_scope.
Lemma SAT_gen_heap_init {Σ: gFunctors} {L V} `{Countable L} `{!gen_heapPreG L V Σ} σ F P:
  SAT F [] P →
  ∃ _: gen_heapG L V Σ,
    SAT F [] (gen_heap_interp σ ∗
                    ([∗ map] l ↦ v ∈ σ, l ↦ v)%I ∗
                    ([∗ map] l ↦ a ∈ σ, meta_token l ⊤) ∗ P)%I.
Proof.
  (* NOTE: this proof follows [gen_heap_init] *)
  intros Hsat.
  eapply (SAT_alloc_res (gmap_view_auth (DfracOwn 1) (∅ : gmap L (leibnizO V)))) in Hsat as [γh Hsat]; last first.
  { exact: gmap_view_auth_valid. }
  eapply (SAT_alloc_res (gmap_view_auth (DfracOwn 1) (∅ : gmap L (gnameO)))) in Hsat as [γm Hsat]; last first.
  { exact: gmap_view_auth_valid. }
  exists (GenHeapG _ _ _ γh γm).
  eapply SAT_upd, SAT_mono, Hsat.
  iIntros "(Hh & Hm & $)".
  iAssert (gen_heap_interp (hG:=GenHeapG _ _ _ γh γm) ∅) with "[Hh Hm]" as "Hinterp".
  { iExists ∅; simpl. iFrame "Hh Hm". by rewrite dom_empty_L. }
  iMod (gen_heap_alloc_big with "Hinterp") as "(Hinterp & $ & $)".
  { apply map_disjoint_empty_r. }
  rewrite right_id_L. done.
Qed.


(* Inv Heaps *)
From transfinite.base_logic.lib Require Import gen_inv_heap.

Lemma SAT_inv_heap_init (L V : Type)
  `{Countable L, !invG Σ, !gen_heapG L V Σ, !inv_heapGpreS L V Σ} E P F :
  SAT F [view E] P →
  ∃ _ : inv_heapG L V Σ, SAT F [view E] (inv_heap_inv L V ∗ P).
Proof.
  (* NOTE: this proof follows [inv_heap_init] *)
  intros Hsat. eapply (SAT_alloc_res (● (to_inv_heap ∅))) in Hsat as [γ Hsat]; last first.
  { rewrite auth_auth_valid. eapply to_inv_heap_valid. }
  exists (Inv_HeapG L V γ).
  eapply SAT_fupd, SAT_mono, Hsat.
  iIntros "[H● $]". iAssert (inv_heap_inv_P (gG := Inv_HeapG L V γ)) with "[H●]" as "P".
  { iExists _; simpl. iFrame. done. }
  iApply (inv_alloc inv_heapN E inv_heap_inv_P with "P").
Qed.

(* Proph Maps *)
From transfinite.base_logic.lib Require Import proph_map.


Lemma SAT_proph_map_init `{Countable P, !proph_mapPreG P V Σ} pvs ps F Q :
  SAT F [] Q →
  ∃ _ : proph_mapG P V Σ, SAT F [] (proph_map_interp pvs ps ∗ Q).
Proof.
  (* NOTE: this proof is based on [proph_map_init] *)
  intros Hsat. eapply (SAT_alloc_res (gmap_view_auth (DfracOwn 1) ∅)) in Hsat as [γ Hsat]; last first.
  { apply gmap_view_auth_valid. }
  exists (ProphMapG _ P V _ _ _ _ γ). eapply SAT_mono, Hsat.
  iIntros "(Hh & $)". iExists ∅; simpl. iFrame. by iPureIntro.
Qed.


(* Mono Nat *)
From iris.algebra Require Import mono_nat.
From transfinite.base_logic.lib Require Import mono_nat.

Lemma SAT_mono_nat_own_alloc `{!mono_natG Σ} n F P :
  SAT F [] P →
  ∃ γ, SAT F [] (mono_nat_auth_own γ 1 n ∗ mono_nat_lb_own γ n ∗ P).
Proof.
  (* NOTE: this proof is based on [mono_nat_own_alloc_strong] *)
  rewrite mono_nat.mono_nat_auth_own_unseal /mono_nat.mono_nat_auth_own_def.
  rewrite mono_nat.mono_nat_lb_own_unseal /mono_nat.mono_nat_lb_own_def.
  intros Hsat. eapply (SAT_alloc_res (●MN n ⋅ ◯MN n)) in Hsat as [γ Hsat]; last first.
  { apply mono_nat_both_valid; auto. }
  exists γ. eapply SAT_mono, Hsat.
  iIntros "([$ $] & $)".
Qed.

