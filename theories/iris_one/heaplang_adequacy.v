From iris.prelude Require Import options.
From iris.algebra Require Import auth.
From iris.heap_lang Require Import lang.
From iris.proofmode Require Import proofmode.
From iris.base_logic.lib Require Import mono_nat.
From iris.base_logic Require Import invariants.

From demo.iris_one.prelims Require Export weakestpre.
From demo.iris_one Require Import satisfiable.
From demo.iris_one Require Import program_logic_adequacy base_logic_extension.
From demo.iris_one.prelims Require Import heaplang_instantiation.

Class heapGpreS (Σ: gFunctors) := HeapGpreS {
  heapGpreS_iris :> invGpreS Σ;
  heapGpreS_heap :> gen_heapGpreS loc (option val) Σ;
  heapGpreS_inv_heap :> inv_heapGpreS loc (option val) Σ;
  heapGpreS_proph :> proph_mapGpreS proph_id (val * val) Σ;
  heapGS_step_cnt :> mono_natG Σ;
}.

Definition heapΣ : gFunctors :=
  #[invΣ; gen_heapΣ loc (option val); inv_heapΣ loc (option val);
    proph_mapΣ proph_id (val * val); mono_natΣ].
Global Instance subG_heapGpreS {Σ} : subG heapΣ Σ → heapGpreS Σ.
Proof. solve_inG. Qed.

Lemma heap_lang_initial_allocation `{!heapGpreS Σ} ns nt σ (_: invGS_gen HasNoLc Σ) (F: iProp Σ) κs:
  SAT F [view ⊤] True →
  ∃ (h: heapGS Σ), let inv: invGS_gen HasNoLc Σ := iris_invGS in (* we ensure that all inferences of [invGS] point to this instance *)
  SAT F [view ⊤] (state_interp σ ns κs nt ∗ inv_heap_inv).
Proof.
  intros Hsat.
  eapply SAT_frame_resource with (R := view _) in Hsat; last apply _.
  eapply (SAT_gen_heap_init σ.(heap)) in Hsat as [Hgen Hsat].
  apply SAT_unframe_resource in Hsat.
  eapply (SAT_inv_heap_init loc (option val)) in Hsat as [Hinv Hsat].
  eapply SAT_frame_resource with (R := view _) in Hsat; last apply _.
  eapply SAT_proph_map_init in Hsat as [Hproph Hsat].
  eapply (SAT_mono_nat_own_alloc ns) in Hsat as [γm Hsat].
  apply SAT_unframe_resource in Hsat.
  pose (hg := (HeapGS _ _ _ _ _ γm _)). exists hg.
  eapply SAT_mono, Hsat.
  iIntros "(Hm & Hn & Hp & Hi & Hgen & Hpts & Hmeta & _)".
  rewrite /state_interp /=. by iFrame.
Qed.


Lemma heap_adequacy Σ `{!heapGpreS Σ} s e σ φ :
  (∀ `{!heapGS Σ}, ⊢ inv_heap_inv -∗ WP e @ s; ⊤ {{ v, ⌜φ v⌝ }}) →
  adequate s e σ (λ v _, φ v).
Proof.
  intros Hwp. eapply SAT_wp_adequate with (P := λ _: heapGS Σ, inv_heap_inv) (nt := 0) (ns := 0) (φ := φ).
  - eapply heap_lang_initial_allocation.
  - intros x; simpl. iIntros "Hi". by iApply Hwp.
Qed.


Lemma heap_adequacy_closed s e σ φ:
  (∀ `{!heapGS heapΣ}, ⊢ inv_heap_inv -∗ WP e @ s; ⊤ {{ v, ⌜φ v⌝ }}) →
  adequate s e σ (λ v _, φ v).
Proof.
  intros Hwp. eapply heap_adequacy, Hwp. apply _.
Qed.

(* Uses the classical assumptions of Transfinite Iris. *)
Print Assumptions heap_adequacy.