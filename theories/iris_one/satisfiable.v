From iris.base_logic Require Export iprop.
From iris.base_logic Require Import own.
From iris.proofmode Require Import tactics.


(** *** The Satisfiable Predicate *)
Definition sat_def {M: ucmra} (P: uPred M) : Prop :=
  ∃ r: M, ✓{1} r ∧ uPred_holds P 1 r.
Definition sat_aux : seal (@sat_def). by eexists. Qed.
Definition sat := sat_aux.(unseal).
Global Arguments sat {M} _%I.
Definition sat_eq : @sat = @sat_def := sat_aux.(seal_eq).
Global Instance: Params (@sat) 1 := {}.


Section satisfiable_properties.
  Context {M: ucmra}.
  Local Arguments uPred_holds {_} !_ _ _ /.

  Lemma sat_intro: sat (M:=M) True.
  Proof.
    rewrite sat_eq /sat_def.
    exists ε. split; first apply ucmra_unit_validN. by uPred.unseal.
  Qed.

  Lemma sat_mono (P Q: uPred M) : (P ⊢ Q) → sat P → sat Q.
  Proof.
    rewrite sat_eq /sat_def.
    intros HPQ HQ. destruct (HQ) as (r & ? & ?). exists r. split; first done.
    destruct HPQ as [HPQ]. by eapply HPQ.
  Qed.

  Lemma sat_elim (φ: Prop): sat (M:=M) ⌜φ⌝ → φ.
  Proof.
    rewrite sat_eq /sat_def.
    intros H. destruct (H) as (r & ? & Hpure). revert Hpure. by uPred.unseal.
  Qed.

  Lemma sat_upd (P: uPred M) : sat (|==> P) → sat P.
  Proof.
    rewrite sat_eq /sat_def.
    intros Hsat. destruct (Hsat) as (r & ? & HP).
    revert HP. uPred.unseal. intros HP.
    destruct (HP 1 ε) as (r' & Hv' & HP'); first done.
    { by rewrite right_id. }
    exists r'. rewrite right_id in Hv'. split; done.
  Qed.

  Lemma sat_exists {A} (Ψ: A → uPred M) :
    sat (∃ x, Ψ x) → ∃ x, sat (Ψ x).
  Proof.
    rewrite sat_eq /sat_def. uPred.unseal.
    intros (r & ? & x & HP). exists x, r.
    split; done.
  Qed.

  (* we need this lemma to eliminate an except zero modality *)
  Lemma sat_later_pure φ : sat (M := M) (▷ ⌜φ⌝) → φ.
  Proof.
    rewrite sat_eq /sat_def. uPred.unseal. naive_solver.
  Qed.


  (** resources *)
  Lemma sat_valid_own (r: M) : ✓ r → sat (uPred_ownM r).
  Proof.
    rewrite sat_eq /sat_def.
    intros Hr. exists r. split; first by eapply cmra_valid_validN.
    uPred.unseal. exists ε. rewrite right_id //.
  Qed.

  Lemma sat_own_valid (r: M) : sat (uPred_ownM r) → ✓{1} r.
  Proof.
    rewrite sat_eq /sat_def.
    intros Hsat. destruct (Hsat) as (r' & ? & Hown). revert Hown. uPred.unseal.
    intros Hown. by eapply cmra_validN_includedN.
  Qed.


  (* Derived Properties *)
  Global Instance sat_proper : Proper ((≡) ==> (iff)) (sat (M := M)).
  Proof.
    intros P Q HPQ. split; eapply sat_mono; rewrite HPQ //.
  Qed.

  Global Instance sat_proper_ent : Proper ((⊢) ==> impl) (sat (M := M)).
  Proof.
    intros P Q HPQ. rewrite /impl. by eapply sat_mono.
  Qed.

  Global Instance sat_proper_flip_ent : Proper (flip (⊢) ==> flip impl) (sat (M := M)).
  Proof.
    intros P Q HPQ. rewrite /flip /impl. by eapply sat_mono.
  Qed.

  (* disjunction holds trivially in the transfinite model *)
  Lemma sat_or (P Q: uPred M) :
    sat (P ∨ Q) → sat P ∨ sat Q.
  Proof.
    rewrite bi.or_alt. intros [[|] Hsat]%sat_exists; eauto.
  Qed.

  Lemma sat_except_0 (P: uPred M):
    sat (◇ P) → sat P.
  Proof.
    intros Hsat. eapply sat_or in Hsat as [Hsat|Hsat]; last done.
    by eapply sat_later_pure in Hsat.
  Qed.

End satisfiable_properties.




(** *** Satisfiable which bakes in a frame and a list of resources. *)
Definition SAT_def {Σ: gFunctors} (F: iProp Σ) (Rs: list (iProp Σ)) (P: iProp Σ) : Prop := sat (F ∗ ([∗] Rs) ∗ P).
Definition SAT_aux : seal (@SAT_def). by eexists. Qed.
Definition SAT := SAT_aux.(unseal).
Global Arguments SAT {Σ} _%I _%I _%I.
Definition SAT_eq : @SAT = @SAT_def := SAT_aux.(seal_eq).
Global Instance: Params (@SAT) 1 := {}.

(* type class for searching for resources in the resource list *)
Class find_resource {Σ: gFunctors} (Rs: list (iProp Σ)) (P: iProp Σ) (Rs': list (iProp Σ)) : Prop :=
  find_resource_iff: ([∗] Rs) ⊣⊢ P ∗ ([∗] Rs').
Global Arguments find_resource {_} _%I _%I _%I.
Global Hint Mode find_resource + + ! - : typeclass_instances.


Section satisfiable_frame_properties.
  Context {Σ: gFunctors}.

  Lemma SAT_intro : SAT (Σ:=Σ) True [] True.
  Proof.
    rewrite SAT_eq /SAT_def.
    eapply sat_mono, sat_intro.
    simpl; by iIntros "_".
  Qed.

  Lemma SAT_mono F Rs (P Q: iProp Σ):
    (P ⊢ Q) → SAT F Rs P → SAT F Rs Q.
  Proof.
    rewrite SAT_eq /SAT_def.
    intros Hent Hsat. eapply sat_mono, Hsat.
    iIntros "($ & $ & P)". by iApply Hent.
  Qed.

  Lemma SAT_elim F Rs (φ: Prop):
    SAT (Σ := Σ) F Rs ⌜φ⌝ → φ.
  Proof.
    rewrite SAT_eq /SAT_def.
    intros Hsat. eapply sat_elim, sat_mono, Hsat.
    iIntros "(_ & _ & $)".
  Qed.

  Lemma SAT_upd F Rs (P: iProp Σ):
    SAT F Rs (|==> P) → SAT F Rs P.
  Proof.
    rewrite SAT_eq /SAT_def.
    intros Hsat. eapply sat_upd, sat_mono, Hsat.
    iIntros "($ & $ & $)".
  Qed.

  Lemma SAT_later_pure F Rs (φ: Prop):
    SAT (Σ := Σ) F Rs (▷ ⌜φ⌝) → φ.
  Proof.
    rewrite SAT_eq /SAT_def.
    intros Hsat. eapply sat_later_pure, sat_mono, Hsat.
    iIntros "(_ & _ & $)".
  Qed.

  Lemma SAT_exists {A} F Rs (Ψ: A → iProp Σ):
    SAT F Rs (∃ x, Ψ x) → ∃ x, SAT F Rs (Ψ x).
  Proof.
    rewrite SAT_eq /SAT_def.
    intros Hsat. eapply sat_exists, sat_mono, Hsat.
    iIntros "($ & $ & $)".
  Qed.

  Lemma SAT_or F Rs (P Q: iProp Σ):
    SAT F Rs (P ∨ Q) → SAT F Rs P ∨ SAT F Rs Q.
  Proof.
    rewrite SAT_eq /SAT_def.
    intros Hsat. eapply sat_or.
    eapply sat_mono, Hsat.
    iIntros "($ & $ & $)".
  Qed.

  Lemma SAT_resource_exchange F F' Rs Rs' (P: iProp Σ):
    F ∗ [∗] Rs ⊣⊢ F' ∗ [∗] Rs' →
    SAT F Rs P → SAT F' Rs' P.
  Proof.
    rewrite SAT_eq /SAT_def.
    intros Hiff Hsat. eapply sat_mono, Hsat.
    rewrite bi.sep_assoc Hiff -bi.sep_assoc //.
  Qed.

  Lemma SAT_frame_resource (R: iProp Σ) F Rs Rs' (P: iProp Σ):
    SAT F Rs P →
    find_resource Rs R Rs' →
    SAT (R ∗ F) Rs' P.
  Proof.
    intros Hsat Hfind. eapply SAT_resource_exchange, Hsat.
    rewrite (@find_resource_iff _ Rs R) - bi.sep_assoc.
    iSplit; iIntros "($ & $ & $)".
  Qed.

  Lemma SAT_unframe_resource (R: iProp Σ) F Rs (P: iProp Σ):
    SAT (R ∗ F) Rs P →
    SAT F (R :: Rs) P.
  Proof.
    intros Hsat. eapply SAT_resource_exchange, Hsat; simpl.
    rewrite -bi.sep_assoc. iSplit; iIntros "($ & $ & $)".
  Qed.


  (* Context Search *)
  Global Instance find_resource_head (R: iProp Σ) Rs :
    find_resource (R :: Rs) R Rs | 1.
  Proof.
    rewrite /find_resource //=.
  Qed.

  Global Instance find_resource_tail (R R': iProp Σ) Rs Rs' :
    find_resource Rs R Rs' →
    find_resource (R' :: Rs) R (R' :: Rs') | 2.
  Proof.
    rewrite /find_resource //=.
    intros ->. iSplit; iIntros "($ & $ & $)".
  Qed.

  Lemma SAT_res_cons F R Rs (P: iProp Σ):
    SAT F (R :: Rs) P ↔ SAT F Rs (R ∗ P).
  Proof.
    rewrite SAT_eq /SAT_def /=. rewrite -!bi.sep_assoc.
    split; eapply sat_mono; iIntros "($ & $ & $ & $)".
  Qed.

  Lemma SAT_frame_cons F R Rs (P: iProp Σ):
    SAT (R ∗ F) Rs P ↔ SAT F Rs (R ∗ P).
  Proof.
    rewrite SAT_eq /SAT_def /=. rewrite -!bi.sep_assoc.
    split; eapply sat_mono; iIntros "($ & $ & $ & $)".
  Qed.


  (* Derived Properties *)
  Lemma SAT_elim_iterated F Rs G n (P: iProp Σ):
    (∀ P, SAT F Rs (G P) → SAT F Rs P) →
    SAT F Rs (Nat.iter n G P) → SAT F Rs P.
  Proof.
    intros Hiterated. induction n as [|n IHn]; eauto.
  Qed.

  Global Instance SAT_proper : Proper ((≡) ==> (≡) ==> (≡) ==> (iff)) (SAT (Σ := Σ)).
  Proof.
    intros F F' HF Rs Rs' HRs P P' HP. rewrite SAT_eq /SAT_def.
    rewrite HF HP. f_equiv. f_equiv. f_equiv.
    induction HRs as [|R R' Rs Rs' Heq _ IH]; simpl; first done.
    rewrite Heq IH //.
  Qed.

  Global Instance SAT_proper_ent F Rs : Proper ((⊢) ==> impl) (SAT (Σ := Σ) F Rs).
  Proof.
    intros P Q HPQ. rewrite /impl. by eapply SAT_mono.
  Qed.

  Global Instance SAT_proper_flip_ent F Rs : Proper (flip (⊢) ==> flip impl) (SAT (Σ := Σ) F Rs).
  Proof.
    intros P Q HPQ. rewrite /flip /impl. by eapply SAT_mono.
  Qed.

  Lemma SAT_except_0 F Rs (P: iProp Σ):
    SAT F Rs (◇ P) → SAT F Rs P.
  Proof.
    intros Hsat. eapply SAT_or in Hsat as [Hsat|Hsat]; last done.
    by eapply SAT_later_pure in Hsat.
  Qed.

  (* in the transfinite model, we can derive this rule, because we can
     just pull out the existential *)
  Lemma SAT_alloc_res {A: cmra} `{!inG Σ A} (a: A) F Rs P:
    ✓ a → SAT F Rs P → ∃ γ, SAT F Rs (own γ a ∗ P).
  Proof.
    intros Hv Hsat.
    eapply SAT_exists, SAT_upd, SAT_mono, Hsat.
    iIntros "$". by iApply own_alloc.
  Qed.

End satisfiable_frame_properties.