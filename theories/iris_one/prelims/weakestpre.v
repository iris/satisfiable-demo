From iris.proofmode Require Import base proofmode classes.
From iris.base_logic.lib Require Export fancy_updates.
From iris.program_logic Require Export language.
(* FIXME: If we import iris.bi.weakestpre earlier texan triples do not
   get pretty-printed correctly. *)
From iris.bi Require Export weakestpre.
From iris.prelude Require Import options.
From iris.bi.lib Require Import fixpoint.
Import uPred.


Class irisGS (Λ : language) (Σ : gFunctors) := IrisGS {
  iris_invGS :> invGS_gen HasNoLc Σ;

  (** The state interpretation is an invariant that should hold in
  between each step of reduction. Here [Λstate] is the global state,
  the first [nat] is the number of steps already performed by the
  program, [list (observation Λ)] are the remaining observations, and the
  last [nat] is the number of forked-off threads (not the total number
  of threads, which is one higher because there is always a main
  thread). *)
  state_interp : state Λ → nat → list (observation Λ) → nat → iProp Σ;

  (** A fixed postcondition for any forked-off thread. For most languages, e.g.
  heap_lang, this will simply be [True]. However, it is useful if one wants to
  keep track of resources precisely, as in e.g. Iron. *)
  fork_post : val Λ → iProp Σ;

  (** When performing pure steps, the state interpretation needs to be
  adapted for the change in the [ns] parameter.

  Note that we use an empty-mask fancy update here. We could also use
  a basic update or a bare magic wand, the expressiveness of the
  framework would be the same. If we removed the modality here, then
  the client would have to include the modality it needs as part of
  the definition of [state_interp]. Since adding the modality as part
  of the definition [state_interp_mono] does not significantly
  complicate the formalization in Iris, we prefer simplifying the
  client. *)
  state_interp_mono σ ns κs nt:
    state_interp σ ns κs nt ⊢ |={∅}=> state_interp σ (S ns) κs nt
}.
Global Opaque iris_invGS.
Global Arguments IrisGS {Λ Σ}.


(** The predicate we take the fixpoint of in order to define the WP. *)
Definition wp_pre `{!irisGS Λ Σ} (s : stuckness)
    (wp : prodO (prodO (discreteO coPset) (expr Λ)) (val Λ -d> iPropO Σ) -> iPropO Σ) :
    prodO (prodO (discreteO coPset) (expr Λ)) (val Λ -d> iPropO Σ) -> iPropO Σ := λ '(E, e1, Φ),
  match to_val e1 with
  | Some v => |={E}=> Φ v
  | None => ∀ σ1 ns κ κs nt,
     state_interp σ1 ns (κ ++ κs) nt ={E,∅}=∗
       ⌜if s is NotStuck then reducible e1 σ1 else True⌝ ∗
       ∀ e2 σ2 efs, ⌜prim_step e1 σ1 κ e2 σ2 efs⌝ -∗
         |={∅,E}=>
         state_interp σ2 (S ns) κs (length efs + nt) ∗
         wp (E, e2, Φ) ∗
         [∗ list] i ↦ ef ∈ efs, wp (⊤, ef, fork_post)
  end%I.

Local Instance wp_pre_mono `{!irisGS Λ Σ} s : BiMonoPred (wp_pre s).
Proof.
  split.
  - (* monotonicity *)
    intros wp wp' Hne Hne'. iIntros "#Hw" ([[E e] Φ]). rewrite /wp_pre /=.
    iIntros "Hwp". destruct (to_val e) as [v|]; first done.
    iIntros (σ1 ns κ κs nt) "Hσ". iMod ("Hwp" with "Hσ") as "[% Hwp]".
    iModIntro. iSplit; first done. iIntros (e2 σ2 efs Hstep).
    iMod ("Hwp" with "[//]") as "($ & Hwp & Hwps)". iModIntro.
    iSplitL "Hwp"; first by iApply "Hw". clear Hstep.
    iInduction efs as [|ef efs] "IH"; simpl; first done.
    iDestruct "Hwps" as "[Hwp Hwps]". iSplitL "Hwp".
    + by iApply "Hw".
    + by iApply "IH".
  - (* nonexpansiveness *)
    rewrite /wp_pre /= => wp Hne n [[E e1] Φ] [[E' e2] Ψ] .
    intros [[Heq1 Heq2] Heq3]; simpl in *.
    apply discrete_iff, leibniz_equiv in Heq1; last apply _.
    apply discrete_iff, leibniz_equiv  in Heq2; last apply _.
    subst. destruct (to_val e2); by repeat f_equiv.
Qed.


Local Definition wp_def `{!irisGS Λ Σ} : Wp (iProp Σ) (expr Λ) (val Λ) stuckness :=
  λ s : stuckness, λ E e Φ, bi_greatest_fixpoint (wp_pre s) (E, e, Φ).
Local Definition wp_aux : seal (@wp_def). Proof. by eexists. Qed.
Definition wp' := wp_aux.(unseal).
Global Arguments wp' {Λ Σ _}.
Global Existing Instance wp'.
Local Lemma wp_unseal `{!irisGS Λ Σ} : wp = @wp_def Λ Σ _.
Proof. rewrite -wp_aux.(seal_eq) //. Qed.


(* We only prove the lemmas relevant for adequacy *)
Section wp.
Context `{!irisGS Λ Σ}.
Implicit Types s : stuckness.
Implicit Types P : iProp Σ.
Implicit Types Φ : val Λ → iProp Σ.
Implicit Types v : val Λ.
Implicit Types e : expr Λ.

(* Weakest pre *)
Lemma wp_unfold s E e Φ :
  WP e @ s; E {{ Φ }} ⊣⊢ wp_pre s (λ '(E, e, Φ), wp (PROP:=iProp Σ) s E e Φ) (E, e, Φ).
Proof.
  rewrite wp_unseal /wp_def.
  rewrite (greatest_fixpoint_unfold (wp_pre s)) //.
Qed.

End wp.
