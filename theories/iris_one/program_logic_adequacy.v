From iris.proofmode Require Import tactics.

From demo.iris_one.prelims Require Export weakestpre.
From demo.iris_one Require Import satisfiable.
From demo.iris_one Require Import base_logic_extension.


(* Threadpool weakest precondition *)
Definition WPTP `{!irisGS Λ Σ} s t Φs : iProp Σ := ([∗ list] e;Φ ∈ t;Φs, WP e @ s; ⊤ {{ Φ }}).

Lemma wp_wptp `{!irisGS Λ Σ} s e Φ:
  WP e @ s; ⊤ {{ Φ }} ⊢ WPTP s [e] [Φ].
Proof. by rewrite /WPTP big_sepL2_singleton. Qed.



(* Compositional Lemmas for Adequacy *)
Section satisfiability_weakest_pre.
  Context `{!irisGS Λ Σ}.

  Lemma wp_step F E e1 σ1 κ e2 σ2 efs Φ ns nt κs s:
    prim_step e1 σ1 κ e2 σ2 efs →
    SAT F [view E]
      (state_interp σ1 ns (κ ++ κs) nt ∗ WP e1 @ s; E {{ Φ }}) →
    SAT F [view E]
      (state_interp σ2 (S ns) κs (length efs + nt) ∗ WP e2 @ s; E {{ Φ }} ∗ ([∗ list] e ∈ efs, WP e @ s; ⊤ {{ fork_post }})).
  Proof.
    intros Hstep Hsat. eapply SAT_mono in Hsat; last first.
    { iIntros "[HSI Hwp]". rewrite wp_unfold /wp_pre.
      rewrite (val_stuck e1 σ1 κ e2 σ2 efs) //.
      iSpecialize ("Hwp" $! σ1 ns κ κs nt with "HSI").
      iExact "Hwp". }
    eapply SAT_fupd in Hsat.
    eapply SAT_mono in Hsat; last first.
    { iIntros "[_ Hwp]". iSpecialize ("Hwp" $! e2 σ2 efs Hstep). iExact "Hwp". }
    eapply SAT_fupd in Hsat. eapply Hsat.
  Qed.

  Lemma wp_not_stuck F E e σ Φ ns nt κs:
    SAT F [view E] (state_interp σ ns κs nt ∗ WP e @ NotStuck; E {{ Φ }}) →
    not_stuck e σ.
  Proof.
    intros Hsat. rewrite /not_stuck. destruct (to_val e) eqn: He; first by eauto.
    right. eapply SAT_mono with (Q := (|={E, ∅}=> ⌜reducible e σ⌝)%I) in Hsat.
    { eapply SAT_fupd in Hsat. by eapply SAT_elim in Hsat. }
    iIntros "[SI Hwp]". rewrite wp_unfold /wp_pre.
    rewrite He. iMod ("Hwp" $! σ ns [] κs nt with "SI") as "[$ _]".
    by iModIntro.
  Qed.

  Lemma wp_postcondition F E s e Φ v:
    SAT F [view E] (WP e @ s; E {{ Φ }}) →
    to_val e = Some v →
    SAT F [view E] (Φ v).
  Proof.
    intros Hsat Hval. eapply SAT_mono in Hsat; last first.
    { iIntros "Hwp". rewrite wp_unfold /wp_pre Hval. iExact "Hwp". }
    by eapply SAT_fupd in Hsat.
  Qed.

  Lemma wptp_length F s Rs es Φs :
    SAT F Rs (WPTP s es Φs) → length es = length Φs.
  Proof.
    intros Hsat. eapply SAT_elim, SAT_mono, Hsat. rewrite /WPTP. by iApply big_sepL2_length.
  Qed.

  Lemma wptp_extract_wp F Rs s es1 e es2 Φs :
    SAT F Rs (WPTP s (es1 ++ e :: es2) Φs) →
    ∃ Φ1s Φ2s Φ, Φs = Φ1s ++ Φ :: Φ2s ∧ length Φ1s = length es1 ∧ length Φ2s = length es2 ∧
      SAT F Rs (WP e @ s; ⊤ {{ Φ }} ∗ WPTP s (es1 ++ es2) (Φ1s ++ Φ2s)).
  Proof.
    intros Hsat. eapply wptp_length in Hsat as Hlen. symmetry in Hlen.
    specialize (lookup_lt_is_Some_2 Φs (length es1)) as [Φ Hrest].
    { rewrite Hlen app_length /=. lia. }
    eapply take_drop_middle in Hrest as Hsplit.
    eexists _, _, _. split_and!; first done.
    - rewrite take_length Hlen app_length /=. lia.
    - rewrite drop_length Hlen app_length /=. lia.
    - eapply SAT_mono, Hsat. rewrite -{1}Hsplit.
      rewrite /WPTP. iIntros "Hx". iDestruct (big_sepL2_app_inv with "Hx") as "[$ Hx2]".
      { rewrite take_length Hlen app_length /=. lia. }
      iDestruct "Hx2" as "[$ $]".
  Qed.

  Lemma wptp_step F s es1 es2 σ1 σ2 κ κs Φs ns nt :
    SAT F [view ⊤] (state_interp σ1 ns (κ ++ κs) nt ∗ WPTP s es1 Φs) →
    step (es1,σ1) κ (es2, σ2) →
    ∃ nt', SAT F [view ⊤] (state_interp σ2 (S ns) κs (nt + nt') ∗ WPTP s es2 (Φs ++ replicate nt' fork_post)).
  Proof.
    intros Hsat Hstep. destruct Hstep as [e1' σ1' e2' σ2' efs t2' t3 Hstep]; simplify_eq/=.
    rewrite -SAT_frame_cons in Hsat.
    eapply wptp_extract_wp in Hsat as (Φ1s & Φ2s & Φ & -> & Hlen1 & Hlen2 & Hsat).
    rewrite SAT_frame_cons in Hsat.
    eapply SAT_mono in Hsat; last first.
    { iIntros "(SI & Hwp & Hwps)". iCombine "Hwps SI Hwp" as "Hx". iExact "Hx". }
    rewrite -SAT_frame_cons in Hsat.
    eapply wp_step in Hsat as Hsat; last done.
    rewrite SAT_frame_cons in Hsat.
    eexists _. eapply SAT_mono, Hsat.
    iIntros "(Hwps & SI & Hwp & Hforks)". rewrite Nat.add_comm. iFrame "SI".
    iDestruct (big_sepL2_app_inv with "Hwps") as "[Hwps1 Hwps2]"; first auto.
    rewrite -app_assoc /=. rewrite /WPTP. iFrame.
    rewrite big_sepL2_replicate_r //.
  Qed.

  Lemma wptp_steps F k s es1 es2 σ1 σ2 κs κs' Φs ns nt :
    SAT F [view ⊤] (state_interp σ1 ns (κs ++ κs') nt ∗ WPTP s es1 Φs) →
    language.nsteps k (es1, σ1) κs (es2, σ2) →
    ∃ nt', SAT F [view ⊤] (state_interp σ2 (ns + k) κs' (nt + nt') ∗ WPTP s es2 (Φs ++ replicate nt' fork_post)).
  Proof.
    induction k as [|k IH] in  es1, σ1, ns, κs, nt, Φs |-*; intros Hsat Hsteps.
    - revert Hsat. inversion_clear Hsteps. exists 0. rewrite !right_id !Nat.add_0_r //.
    - revert Hsat. inversion_clear Hsteps as [|?? [t1' σ1']]. rewrite -app_assoc. intros Hsat.
      eapply wptp_step in Hsat as (nt' & Hsat); last done.
      eapply IH in Hsat as (nt'' & Hsat); last done.
      exists (nt' + nt''). rewrite !Nat.add_assoc.
      revert Hsat. rewrite -app_assoc replicate_add //= Nat.add_succ_r //.
  Qed.

  Lemma wptp_not_stuck F e es σ ns κs nt Φs:
    SAT F [view ⊤] (state_interp σ ns κs nt ∗ WPTP NotStuck es Φs) →
    e ∈ es →
    not_stuck e σ.
  Proof.
    intros Hsat [i Hlook]%elem_of_list_lookup_1.
    eapply take_drop_middle in Hlook as Hsplit.
    rewrite -Hsplit in Hsat. rewrite -SAT_frame_cons in Hsat.
    eapply wptp_extract_wp in Hsat as (Φ1s & Φ2s & Φ & -> & Hlen1 & Hlen2 & Hsat).
    rewrite SAT_frame_cons in Hsat.
    eapply SAT_mono in Hsat; last first.
    { iIntros "(SI & Hwp & Hwps)". iCombine "Hwps SI Hwp" as "Hx". iExact "Hx". }
    rewrite -SAT_frame_cons in Hsat.
    by eapply wp_not_stuck in Hsat.
  Qed.


  Lemma wptp_postconditions F s es Φs:
    SAT F [view ⊤] (WPTP s es Φs) →
    SAT F [view ⊤] ([∗ list] e;Φ ∈ es; Φs, from_option Φ (WP e @ s; ⊤ {{ Φ }}) (to_val e)).
  Proof.
    induction es as [|e es IH] in Φs, F |-*; intros Hsat.
    - eapply wptp_length in Hsat as Hlen. destruct Φs; last done. eapply Hsat.
    - eapply wptp_length in Hsat as Hlen. destruct Φs as [|Φ Φs]; first done.
      revert Hsat. rewrite /WPTP /=. intros Hsat.
      rewrite -SAT_frame_cons. eapply IH. rewrite SAT_frame_cons.
      rewrite bi.sep_comm. revert Hsat. rewrite bi.sep_comm.
      rewrite -!SAT_frame_cons. destruct (to_val e) eqn:Heq.
      + intros Hsat. eapply SAT_mono, wp_postcondition; eauto.
      + eapply SAT_mono. by iIntros "$".
  Qed.


  (* composing the adequacy lemmas *)
  Lemma wptp_adequacy F k s es1 es2 nt ns κs κs' σ1 σ2 Φs:
    SAT F [view ⊤] (state_interp σ1 ns (κs ++ κs') nt ∗ WPTP s es1 Φs) →
    language.nsteps k (es1, σ1) κs (es2, σ2) →
    ∃ nt', SAT F [view ⊤]
      (state_interp σ2 (ns + k) κs' (nt + nt') ∗ ([∗ list] e;Φ ∈ es2; (Φs ++ replicate nt' fork_post), from_option Φ (WP e @ s; ⊤ {{ Φ }}) (to_val e)))
    ∧ (∀ e, s = NotStuck → e ∈ es2 → not_stuck e σ2).
  Proof.
    intros Hsat Hsteps. eapply wptp_steps in Hsat as (nt' & Hsat); last done.
    eexists _. split.
    - rewrite -SAT_frame_cons in Hsat. eapply wptp_postconditions in Hsat.
      rewrite SAT_frame_cons in Hsat. eauto using wptp_not_stuck.
    - intros e -> Hel. eapply wptp_not_stuck; eauto.
  Qed.


  Lemma wp_adequacy F κs s e es σ1 σ2 φ nt ns k :
    (* if we can prove satisfiable of a weakest pre and the state interpretation *)
    SAT F [view ⊤] (state_interp σ1 nt κs ns ∗ WP e @ s; ⊤ {{ v, ⌜φ v⌝%I }}) →
    (* and we take a k-step execution to [e'] and some forked of threads *)
    language.nsteps k ([e], σ1) κs (es, σ2) →
    (* then no thread is stuck and if the main thread terminates in a value, it satisfies the postcondition *)
    (∀ v es', es = language.of_val v :: es' → φ v) ∧
    (∀ e, s = NotStuck → e ∈ es → not_stuck e σ2).
  Proof.
    intros Hsat Hsteps. rewrite wp_wptp in Hsat.
    replace κs with (κs ++ []) in Hsat by rewrite app_nil_r //.
    eapply wptp_adequacy in Hsat as (nt' & Hsat); eauto.
    destruct Hsat as (Hsat & Hnstuck). split; last eapply Hnstuck.
    intros v es' ->; simpl in *. rewrite to_of_val /= in Hsat.
    eapply SAT_elim, SAT_mono, Hsat. iIntros "(_ & $ & _)".
  Qed.

End satisfiability_weakest_pre.



(* Lemma for handling the allocation of invariants and later credits. *)
(* To use it, you should:
    - pick a type X that carries all of the global ghost names for your language (e.g., [heapGS] for HeapLang)
    - pick a function [I] that takes the ghost names and produces an Iris instance
    - prove that you can allocate the initial state interpretation for some [x: X]
    - prove a weakest precondition for all choices of [x: X]

  Then you obtain the result of [wp_adequacy] for your choice of [X] and [I]. *)
Lemma SAT_wp_adequacy {Σ: gFunctors} {Λ} `{!invGpreS Σ} (X: Type) (I: X → irisGS Λ Σ) κs σ1 σ2 ns nt s e es φ k P:
  (* allocate the initial state interpretation *)
  (∀ (iv: invGS_gen HasNoLc Σ) (F: iProp Σ), SAT F [view ⊤] True →
   ∃ (x: X),
    let i: irisGS Λ Σ := I x in
    let inv: invGS_gen HasNoLc Σ := iris_invGS in (* we ensure that all inferences of [invGS] point to this instance *)
    SAT F [view ⊤] (state_interp σ1 nt κs ns ∗ P x)) →
  (* prove the weakest precondition for all choices of [X] *)
  (∀ x, let i: irisGS Λ Σ := I x in P x ⊢ WP e @ s; ⊤ {{ v, ⌜φ v⌝%I }}) →
  (* then any k-step execution is safe: *)
  language.nsteps k ([e], σ1) κs (es, σ2) →
  (∀ v es', es = language.of_val v :: es' → φ v) ∧
  (∀ e, s = NotStuck → e ∈ es → not_stuck e σ2).
Proof.
  intros Halloc Hwp Hsteps.
  pose proof (SAT_intro (Σ := Σ)) as Hsat.
  eapply SAT_alloc_fancy_updates in Hsat as [Hi Hsat].
  eapply (Halloc Hi True%I) in Hsat as (x & Hsat); eauto; simpl in *.
  eapply SAT_mono in Hsat; last first.
  { iIntros "SI". iPoseProof (Hwp x) as "Hwp". iCombine "SI Hwp" as "Hx". iExact "Hx". }
  eapply (@wp_adequacy _ Σ (I x)), Hsteps.
  eapply SAT_mono, Hsat.
  { iIntros "([$ P] & Hwp)". by iApply "Hwp". }
Qed.


(* Definition of adequacy *)
Record adequate {Λ} (s : stuckness) (e1 : expr Λ) (σ1 : state Λ)
    (φ : val Λ → state Λ → Prop) := {
  adequate_result t2 σ2 v2 :
   rtc erased_step ([e1], σ1) (of_val v2 :: t2, σ2) → φ v2 σ2;
  adequate_not_stuck t2 σ2 e2 :
   s = NotStuck →
   rtc erased_step ([e1], σ1) (t2, σ2) →
   e2 ∈ t2 → not_stuck e2 σ2
}.

Lemma adequate_alt {Λ} s e1 σ1 (φ : val Λ → state Λ → Prop) :
  adequate s e1 σ1 φ ↔ ∀ t2 σ2,
    rtc erased_step ([e1], σ1) (t2, σ2) →
      (∀ v2 t2', t2 = of_val v2 :: t2' → φ v2 σ2) ∧
      (∀ e2, s = NotStuck → e2 ∈ t2 → not_stuck e2 σ2).
Proof.
  split.
  - intros []; naive_solver.
  - constructor; naive_solver.
Qed.

Lemma SAT_wp_adequate {Σ: gFunctors} {Λ} `{!invGpreS Σ} (X: Type) (I: X → irisGS Λ Σ) σ1 ns nt s e φ P:
  (* allocate the initial state interpretation *)
  (∀ (iv: invGS_gen HasNoLc Σ) (F: iProp Σ) κs, SAT F [view ⊤] True →
   ∃ (x: X),
    let i: irisGS Λ Σ := I x in
    let inv: invGS_gen HasNoLc Σ := iris_invGS in (* we ensure that all inferences of [invGS] point to this instance *)
    SAT F [view ⊤] (state_interp σ1 nt κs ns ∗ P x)) →
  (* prove the weakest precondition for all choices of [X] *)
  (∀ x, let i: irisGS Λ Σ := I x in P x ⊢ WP e @ s; ⊤ {{ v, ⌜φ v⌝%I }}) →
  adequate s e σ1 (λ v _, φ v).
Proof.
  intros Halloc Hwp.
  apply adequate_alt; intros t2 σ2' [k [κs Hsteps]]%erased_steps_nsteps.
  eapply SAT_wp_adequacy; eauto.
Qed.




